# SWAY setup dotfiles #
![screenshots](https://i.redd.it/gd5pzdd4z4w71.jpg)

### What are you using? ###
* Swaywm - window manager
* swaylock - locking my desktop
* waybar - the 'top' bar
* dunst - notifications
* rofi - quick menu
* alacritty - terminal emulator
* spicetify - spotify themeing


`sudo pacman -S sway swaylock waybar dunst rofi alacritty`
`sudo pacman -S grim slurp pulseaudio light`


### How do I get set up? ###

I assume you already have the packages described above 
you just need to copy the config files in your ~/.config/


